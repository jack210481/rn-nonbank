import React, { Component } from 'react';
import {
    AppRegistry,
} from 'react-native';
import NonBank from './NonBank/index';

AppRegistry.registerComponent('NonBank', () => NonBank);
