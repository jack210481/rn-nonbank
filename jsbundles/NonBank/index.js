import React, { PureComponent } from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';
import {
    JDText
} from '@areslabs/ares-core-lib';

export default class NonBank extends PureComponent{
  render() {
    return (
      <View style={styles.container}>
          <JDText>Hello, non-bank</JDText>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})

module.exports = NonBank;
